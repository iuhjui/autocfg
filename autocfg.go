package autocfg

type AutoCFG struct {
	Generate func() *Config
	Verify   func(*Config) error
}
