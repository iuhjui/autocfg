package autocfg

import (
	"reflect"
	"strings"

	"github.com/spf13/cast"
)

type Config struct {
	data map[interface{}]interface{}
}

func NewConfig(datas ...map[interface{}]interface{}) *Config {
	if len(datas) > 0 {
		return &Config{data: datas[0]}
	}
	return &Config{}
}

func LoadConfig(cfgname string, auto *AutoCFG) (*Config, error) {
	exists, err := PathExists(cfgname)
	if err != nil {
		return nil, err
	}
	if exists == false {
		c := auto.Generate()
		err := WriteFile(cfgname, c, auto)
		if err != nil {
			return nil, err
		}
	}
	c, err := ReadFile(cfgname, auto)
	if err != nil {
		return nil, err
	}
	return c, nil
}

func (cfg *Config) Get(name string) interface{} {
	path := strings.Split(name, ".")
	data := cfg.data
	for index, key := range path {
		value, ok := data[key]
		if !ok {
			break
		}
		if (index + 1) == len(path) {
			return value
		}
		if reflect.TypeOf(value).String() == "map[interface {}]interface {}" {
			data = value.(map[interface{}]interface{})
		}
	}
	return nil
}

func (cfg *Config) GetString(name string) string {
	value := cfg.Get(name)
	return cast.ToString(value)
}

func (cfg *Config) ToStringSlice(name string) []string {
	value := cfg.Get(name)
	return cast.ToStringSlice(value)
}

func (cfg *Config) GetInt(name string) int {
	value := cfg.Get(name)
	return cast.ToInt(value)
}

func (cfg *Config) GetBool(name string) bool {
	value := cfg.Get(name)
	return cast.ToBool(value)
}

func (cfg *Config) GetFloat64(name string) float64 {
	value := cfg.Get(name)
	return cast.ToFloat64(value)
}
