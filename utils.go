package autocfg

import (
	"errors"
	"io/ioutil"
	"os"
	"strings"

	"gopkg.in/yaml.v2"
)

func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func GuessFileType(path string) string {
	parts := strings.Split(path, ".")
	ext := parts[len(parts)-1]
	switch ext {
	case "yaml", "yml":
		return "yaml"
	}
	return ""
}

func ReadFile(cfgname string, auto *AutoCFG) (*Config, error) {
	ext := GuessFileType(cfgname)
	if ext == "" {
		return nil, errors.New("Can not load " + cfgname + " config")
	}
	raw, err := ioutil.ReadFile(cfgname)
	if err != nil {
		return nil, err
	}
	c, err := UnmarshalConfig(raw, auto)
	if err != nil {
		return nil, err
	}
	return c, nil
}

func WriteFile(cfgname string, c *Config, auto *AutoCFG) error {
	buf, err := MarshalConfig(c, auto)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(cfgname, buf, 0755)
	if err != nil {
		return err
	}
	return nil
}

func MarshalConfig(c *Config, auto *AutoCFG) ([]byte, error) {
	if auto.Verify != nil {
		err := auto.Verify(c)
		if err != nil {
			return nil, err
		}
	}
	raw, err := yaml.Marshal(c.data)
	if err != nil {
		return nil, err
	}
	return raw, nil
}

func UnmarshalConfig(buf []byte, auto *AutoCFG) (*Config, error) {
	data := map[interface{}]interface{}{}
	err := yaml.Unmarshal(buf, &data)
	if err != nil {
		return nil, err
	}
	c := NewConfig(data)
	if auto.Verify != nil {
		err := auto.Verify(c)
		if err != nil {
			return nil, err
		}
	}
	return c, nil
}
